package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class Adapter extends ISensor {

    private LegacyTemperatureSensor LS;
    private double value;
    
    public Adapter(LegacyTemperatureSensor LS)
    {
    	this.LS=LS;
    }
    
    public void on()
    {
		if (!LS.getStatus())
		{
			LS.onOff();
		}
	}
    public void off()
    {
    	if (LS.getStatus())
		{
			LS.onOff();
		}
	}
    public boolean getStatus()
    {
    	return LS.getStatus();
    }
    public void update() throws SensorNotActivatedException
    {
    	this.value = LS.getTemperature();
    }

    public double getValue() throws SensorNotActivatedException
    {
    	return LS.getTemperature();
    }
}
