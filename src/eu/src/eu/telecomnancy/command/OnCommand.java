package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;

public class OnCommand implements SensorCommand{

	@Override
	public void applyCommand(ISensor sensor) {
		sensor.on();
	}
}
