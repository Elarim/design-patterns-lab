package eu.telecomnancy.command;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class UpdateCommand implements SensorCommand{

	@Override
	public void applyCommand(ISensor sensor){
		try {
			sensor.update();
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
