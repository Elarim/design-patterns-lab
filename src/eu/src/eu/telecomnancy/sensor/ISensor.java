package eu.telecomnancy.sensor;

import java.util.ArrayList;

public abstract class ISensor {
	private ArrayList<SensorListener> listeners = new ArrayList<SensorListener>();
    /**
     * Enable the sensor.
     */
    public abstract void on();

    /**
     * Disable the sensor.
     */
    public abstract void off();

    /**
     * Get the status (enabled/disabled) of the sensor.
     *
     * @return the current sensor's status.
     */
    public abstract boolean getStatus();

    /**
     * Tell the sensor to acquire a new value.
     *
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public abstract void update() throws SensorNotActivatedException;

    /**
     * Get the latest value recorded by the sensor.
     *
     * @return the last recorded value.
     * @throws SensorNotActivatedException if the sensor is not activated.
     */
    public abstract double getValue() throws SensorNotActivatedException;
    
    public void addListener(SensorListener x)
    {
    	listeners.add(x);
    }
    
    public void notifyListeners()
    {
    	for (int i = 0; i<listeners.size();i++)
    		listeners.get(i).valueChanged();
    }
}
