package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorOn implements SensorState {
	private StateSensor sensor;
	private double value = 0; 
	
	public SensorOn(StateSensor sensor)
	{
		this.sensor = sensor;
	}
	
	@Override
	public void on() {
	}

	@Override
	public void off() {
		sensor.setState(new SensorOff(sensor));
	}

	@Override
	public boolean getStatus() {
		return true;		
	}

	@Override
	public void update() throws SensorNotActivatedException{
		value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() throws SensorNotActivatedException{
		return value;
		
	}

	
}
