package eu.telecomnancy.sensor;


public class StateSensor extends ISensor {
    SensorState state;
    double value = 0;
    
    public StateSensor()
    {
    	state = new SensorOff(this);
    }
    
    @Override
    public void on() {
       state.on();
    }

    @Override
    public void off() {
       state.off();
    }

    @Override
    public boolean getStatus() {
        return state.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        state.update();    
    	notifyListeners();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	return state.getValue();
    }
    
    public void setState(SensorState state)
    {
    	this.state = state; 
    }
}

