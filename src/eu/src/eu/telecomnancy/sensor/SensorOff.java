package eu.telecomnancy.sensor;

import java.util.Random;

public class SensorOff implements SensorState {
	private StateSensor sensor;
	
	public SensorOff(StateSensor sensor)
	{
		this.sensor = sensor;
	}
	
	@Override
	public void on() {
		sensor.setState(new SensorOn(sensor));
	}

	@Override
	public void off() {
	}

	@Override
	public boolean getStatus() {
		return true;		
	}

	@Override
	public void update() throws SensorNotActivatedException{
		throw new SensorNotActivatedException("Sensor must be activated before update value.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}
}

