package eu.telecomnancy.sensor;

import java.sql.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:07
 */
public class SensorProxy extends ISensor {
    private ISensor sensor;
    private SensorLogger log;

    public SensorProxy(ISensor _sensor, SensorLogger sensorLogger) {
        sensor = _sensor;
        log = sensorLogger;
    }

    @Override
    public void on() {
        log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + "on");
        sensor.on();
    }

    @Override
    public void off() {
        log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + "off");
        sensor.off();       
    }

    @Override
    public boolean getStatus() {
        log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + "getStatus" +sensor.getStatus());
        System.out.println("Sensor getStatus");
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + "update");
        System.out.println("Sensor update");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        log.log(LogLevel.INFO, new Date(System.currentTimeMillis()) + "get value =" + sensor.getValue());
        System.out.println("Sensor value =" + sensor.getValue());
        return sensor.getValue();
    }
}
