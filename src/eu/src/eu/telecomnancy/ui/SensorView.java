package eu.telecomnancy.ui;

import eu.telecomnancy.command.OffCommand;
import eu.telecomnancy.command.OnCommand;
import eu.telecomnancy.command.UpdateCommand;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorListener;

import java.util.Observer;
import java.util.Observable;

import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SensorView extends JPanel implements SensorListener {
	
    private ISensor sensor;
    private SensorDisplay display;
    private SensorDisplayLabel displayLabel;
   
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
	private JComboBox change = new JComboBox(new String[]{"Celsius", "Farenheit", "Entier"});
    
    public SensorView(ISensor c) {
        this.sensor = c;
        this.setLayout(new BorderLayout());
       this.sensor.addListener(this);
       
       display = new SensorDisplay(null);
       displayLabel = new SensorDisplayLabel(display);
       
        displayLabel.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        displayLabel.setFont(sensorValueFont);


        
        this.add(displayLabel, BorderLayout.CENTER);
        

        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new OnCommand().applyCommand(sensor);
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	new OffCommand().applyCommand(sensor);
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new UpdateCommand().applyCommand(sensor);
            }
        });
        
        change.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if (change.getSelectedItem().equals("Celsius")){
        			display = new SensorDisplay (display.getCelsius());
        			displayLabel.setSensorDisplay(display);
        		}
        		else if (change.getSelectedItem().equals("Farenheit")){
        			display = new SensorDisplayFarenheit (display.getCelsius());
        			displayLabel.setSensorDisplay(display);
        		}
        		else if (change.getSelectedItem().equals("Entier")){
        			display = new SensorDisplayEntier (display.getCelsius());
        			displayLabel.setSensorDisplay(display);
        		}
        		displayLabel.update();
        	}
        });
        
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(change);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

	@Override
	public void valueChanged() {
		try {
			display.setValue(sensor.getValue());
		} catch (SensorNotActivatedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	displayLabel.update();
	}
}
