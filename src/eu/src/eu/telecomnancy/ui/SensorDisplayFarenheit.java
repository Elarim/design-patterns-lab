package eu.telecomnancy.ui;

public class SensorDisplayFarenheit extends SensorDisplay {
	
	public SensorDisplayFarenheit(Double val) {
		super(val);
	}

	@Override
	public String getValue(){
		if (getCelsius()!=null)
			return ""+(getCelsius()*1.8 + 32);
		else return "N/A";
	}
	
	@Override
	public String getUnit(){
		return "�F";
	}
}


