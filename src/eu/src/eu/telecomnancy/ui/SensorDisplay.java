package eu.telecomnancy.ui;

public class SensorDisplay {
	protected Double val;
	protected SensorDisplayLabel displayLabel;
	
	public SensorDisplay(Double val) {
		this.val = val;
	}
	
	public Double getCelsius(){
		return val;
	}
	
	public String getValue(){
		if (getCelsius()!=null)
			return ""+getCelsius();
		else return "N/A";
	}
	
	public void setValue(Double val){
		this.val = val;
	}
	
	public String getUnit(){
		return "�C";
	}
	
	public void setDisplayLabel(SensorDisplayLabel displayLabel){
		this.displayLabel = displayLabel;
	}
}

