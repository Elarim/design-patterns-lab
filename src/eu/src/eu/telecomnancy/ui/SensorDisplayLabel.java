package eu.telecomnancy.ui;

import javax.swing.JLabel;

public class SensorDisplayLabel extends JLabel {

	public SensorDisplay display;
	
	public SensorDisplayLabel(SensorDisplay display){
		this.display = display;
		if (display.getValue() != null)
		{
			setText(display.getValue() + display.getUnit());
		}
		else setText("N/A °C");
		display.setDisplayLabel(this);
	}
	
	public void setSensorDisplay(SensorDisplay display){
		this.display = display;
	}
	
	public SensorDisplay getSensorDisplay(){
		return display;
	}
	
	public void update(){
		setText(""+display.getValue()+display.getUnit());
	}
	
}
