package eu.telecomnancy.ui;

public class SensorDisplayEntier extends SensorDisplay {

	public SensorDisplayEntier(Double val) {
		super(val);
	}
	
	@Override
	public String getValue(){
		if (getCelsius()!=null)
			return ""+(int)(double) val;
		else return "N/A";
	}
}
