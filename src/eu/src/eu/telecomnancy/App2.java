package eu.telecomnancy;

import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class App2 {

    public static void main(String[] args) {
        Adapter sensor = new Adapter(new LegacyTemperatureSensor());
        new ConsoleUI(sensor);
    }
}